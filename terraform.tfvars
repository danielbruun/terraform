# VMware vSphere configuration
#===============================================================================
# vCenter IP or FQDN #
vsphere_vcenter = "vc.vspherelab.local"
# vSphere username used to deploy the infrastructure #
vsphere_user = "administrator@vspherelab.local"
# Skip the verification of the vCenter SSL certificate (true/false) #
vsphere_unverified_ssl = "true"
# vSphere datacenter name where the infrastructure will be deployed #
vsphere_datacenter = "vspherelab"
# vSphere cluster name where the infrastructure will be deployed #
vsphere_cluster = "vccluster"
# vSphere cluster name where the infrastructure will be deployed #
vsphere_password = "Kompasset123!"



#===============================================================================
# Virtual machine parameters
#===============================================================================
# The name of the virtual machine #
vm_count = "4"
# Timeout of provisioning #
vm_time = "60"
# The name of the virtual machine #
vm_name = "terratest"
# SCSI-controller type #
scsi_type = "lsilogic"
# Firmware boot #
vm_firmware = "efi"
# The datastore name used to store the files of the virtual machine #
vm_datastore = "Datastore3"
# The vSphere network name used by the virtual machine #
vm_network = "VM Network"
# The netmask used to configure the network card of the virtual machine (example: 24) #
vm_netmask = "24"
# The network gateway used by the virtual machine #
vm_gateway = "192.168.200.1"
# The DNS server used by the virtual machine #
vm_dns = "192.168.200.210"
# The domain name used by the virtual machine #
vm_domain = "vspherelab.local"
# The domain username  #
vm_domain_user = "administrator@vspherelab.local"
# The domain users password #
vm_domain_password = "test123!"
# The vSphere template the virtual machine is based on #
vm_template = "packerw19"
# Use linked clone (true/false)
vm_linked_clone = "false"
# The number of vCPU allocated to the virtual machine #
vm_cpu = "1"
# The amount of RAM allocated to the virtual machine #
vm_ram = "2048"
# The IP address of the virtual machine with possibilty for multiple for vms #
vm_ip = "192.168.200."